package ita.benchmark.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PiDigitsDto {

    private long number;

}
