package ita.benchmark.dto;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class BubbleSortDto {

    private int arr[];

}
