package ita.benchmark.service;

import org.springframework.stereotype.Service;

@Service
public class SortService {

    public double calculatePiDigits(long n) {
        double sum = 0.0;
        double flip = -1.0;
        for (long i = 1; i <= n; i++) {
            flip *= -1.0;
            sum += flip / (2 * i - 1);
        }

        return sum * 4.0;
    }

    public int[] doBubbleSort(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return arr;
    }

}
