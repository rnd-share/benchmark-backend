package com.encrypt.service;


import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;
@ApplicationScoped

public class DecryptionService {

    public String decrypt(String encrypted_message, String SECRET_KEY, String SALT) {
        try {
            byte[] raw = Base64.getDecoder().decode(encrypted_message);

            //split raw
            byte[] iv = new byte[16];
            byte[] text = new byte[raw.length - iv.length];
            ByteBuffer bb = ByteBuffer.wrap(raw);
            bb.get(iv,0,iv.length);
            bb.get(text,0, text.length);
            String strToDecrypt = new String(text, StandardCharsets.UTF_8);

            //unwrap secret key
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(SECRET_KEY.toCharArray(), SALT.getBytes(), 278789, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            //decrypt text
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            IvParameterSpec ivspec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivspec);

            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));

        } catch (Exception e) {
            throw new RuntimeException("Error while decrypting with message: " + e.getMessage());
        }
    }

}
