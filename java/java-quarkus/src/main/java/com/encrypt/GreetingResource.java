package com.encrypt;

import com.encrypt.dto.DataDTO;
import com.encrypt.service.*;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import javax.annotation.security.PermitAll;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.core.Response;
@RequestScoped
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GreetingResource {
    @Inject
    Services services;


    @Inject
     EncryptionService encryptionService;

    @Inject
     DecryptionService decryptionService;


    private static final String SECRET_KEY = "M:BDru+g(5Y.Mm]bzv9w]R@Sm[&U/3g9u6b;xwW7dqin3C[K,m28mrhJ]=n/CH!pmSLKmL/_B{g&[tDUSq*{c3!3T@#d.QjNnaw4&m_9J.C=+68,kZbKjVNFGp4-2dBVDN)eJpN48;?yuQxv/RE:%a,Tk=.#BbHt7@2..6Z(?MRV-nXVP6(=ARdx;dLiV+TtDZPy]8(7fR@tD4brJR7&Hg66Bdj*{:i/Q=HKpEAhf5pbR9m%LxB)cnUUj2R)X%4";

    private static final String SALT = "HKA_eVh-%w+x5VVZf,Qdrq9[EYVd:AQKbj$cb@9bh2dL6@@@eRBQQi(/)H[Yr&]c!i5Crv}[[UNZxZfKy+w_x;}FYbg9RtEdd4DF5kt.Tk3[3[_wGz*;jMRK9L;P$;9gaFb,Cj?AtQ+tcWW!zk:-=]CFL!!abbvxTE7y.]K}zC_$xkJqfr8pMry%4h/t?#4.DD6G}3g2x2P]g/Mf9=.]k??L#@YFbzgU3HZdaLkT/Q;YC;y%V+=c%y/;wu{6qnT";



    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/encrypt")
    @PermitAll.
    public String Encrypt(String plainText) {
        System.out.println(plainText);
        String encryptedText = encryptionService.encrypt(plainText, plainText, plainText);
        String decryptedText = decryptionService.decrypt(encryptedText, plainText, plainText);

        if (plainText.equals(decryptedText)) {
            return Response.ok("Encryption and Decryption success").build().toString();
        } else {
            return Response.notModified("Encryption-Decryption process failed").build().toString();
        }

    }



    @POST
    @Path("/pidigits")
    @PermitAll
    public double pidigits(@Valid DataDTO dataDTO) {
        double result = services.calculatePiDigits(dataDTO.getNumber());
        return result;
    }

    @POST
    @Path("/sort")
    @PermitAll
    public int[] sort(@Valid DataDTO dataDTO) {
        int[] result = services.doBubbleSort(dataDTO.getArr());
        return result;
    }
    @GET
    @Path("/plain")
    @Produces(MediaType.TEXT_PLAIN)
    public String plain() {
        return "success";
    }

}