package ita.benchmark.controller;

import ita.benchmark.dto.BubbleSortDto;
import ita.benchmark.dto.PiDigitsDto;
import ita.benchmark.service.DecryptionService;
import ita.benchmark.service.EncryptionService;
import ita.benchmark.service.SortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1")
public class Controller {

    @Autowired
    private SortService service;

    @Autowired
    private EncryptionService encryptionService;

    @Autowired
    private DecryptionService decryptionService;

    private static final String SECRET_KEY = "M:BDru+g(5Y.Mm]bzv9w]R@Sm[&U/3g9u6b;xwW7dqin3C[K,m28mrhJ]=n/CH!pmSLKmL/_B{g&[tDUSq*{c3!3T@#d.QjNnaw4&m_9J.C=+68,kZbKjVNFGp4-2dBVDN)eJpN48;?yuQxv/RE:%a,Tk=.#BbHt7@2..6Z(?MRV-nXVP6(=ARdx;dLiV+TtDZPy]8(7fR@tD4brJR7&Hg66Bdj*{:i/Q=HKpEAhf5pbR9m%LxB)cnUUj2R)X%4";

    private static final String SALT = "HKA_eVh-%w+x5VVZf,Qdrq9[EYVd:AQKbj$cb@9bh2dL6@@@eRBQQi(/)H[Yr&]c!i5Crv}[[UNZxZfKy+w_x;}FYbg9RtEdd4DF5kt.Tk3[3[_wGz*;jMRK9L;P$;9gaFb,Cj?AtQ+tcWW!zk:-=]CFL!!abbvxTE7y.]K}zC_$xkJqfr8pMry%4h/t?#4.DD6G}3g2x2P]g/Mf9=.]k??L#@YFbzgU3HZdaLkT/Q;YC;y%V+=c%y/;wu{6qnT";

    @PostMapping("/encrypt")
    public ResponseEntity<?> doEncryptionDecryption(@RequestBody String plaintext) {
        String encryptedText = encryptionService.encrypt(plaintext, SECRET_KEY, SALT);
        String decryptedText = decryptionService.decrypt(encryptedText, SECRET_KEY, SALT);

        if (plaintext.equals(decryptedText)) {
            return new ResponseEntity<>(encryptedText, HttpStatus.OK);
//            return ResponseEntity.ok("Encryption and Decryption success");
        } else {
            return new ResponseEntity<>("Encryption-Decryption process failed", HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/pidigits")
    public ResponseEntity<?> calculatePiDigits(@RequestBody PiDigitsDto piDigitsDto) {
        return ResponseEntity.ok(service.calculatePiDigits(piDigitsDto.getNumber()));
    }

    @PostMapping("/sort")
    public ResponseEntity<?> bubbleSort(@RequestBody BubbleSortDto bubbleSortDto) {
        return ResponseEntity.ok(service.doBubbleSort(bubbleSortDto.getArr()));
    }

    @GetMapping("/plain")
    public ResponseEntity<?> returnPlain() {
        return ResponseEntity.ok("success");
    }

}
