import { Request, Response } from "express";
import { Cipher, CipherKey, createCipheriv, createDecipheriv, pbkdf2, randomBytes } from "crypto";
import { HttpStatusCode } from "axios";

export class BenchmarkController {

    public bubbleSort (req: Request, res: Response) {
        const arr: number[] = req.body.arr;
        if (!arr) {
            res.status(HttpStatusCode.BadRequest);
        }

        const n = arr.length;

        for (let i = 0; i < n-1; i++) {
            for (let j = 0; j < n-i-1; j++) {
                if (arr[j] > arr[j+1]) {
                    [arr[j], arr[j+1]] = [arr[j+1], arr[j]]
                }
            }
        }

        res.status(200).json({
            arr: arr
        });
    }

    public piDigits (req: Request, res: Response) {
        const numOfIterations: number = req.body.number;
        if (!numOfIterations) {
            res.status(HttpStatusCode.BadRequest);
        }

        let sum = 0;
        let flip = -1;

        for (let i = 1; i <= numOfIterations; i++) {
            flip *= -1;
            sum += flip / (2 * i - 1);
        }

        res.status(200).json({
            res: sum * 4
        });
    }

    public aes (req: Request, res: Response) {
        const secretKey: Buffer = Buffer.from("M:BDru+g(5Y.Mm]bzv9w]R@Sm[&U/3g9u6b;xwW7dqin3C[K,m28mrhJ]=n/CH!pmSLKmL/_B{g&[tDUSq*{c3!3T@#d.QjNnaw4&m_9J.C=+68,kZbKjVNFGp4-2dBVDN)eJpN48;?yuQxv/RE:%a,Tk=.#BbHt7@2..6Z(?MRV-nXVP6(=ARdx;dLiV+TtDZPy]8(7fR@tD4brJR7&Hg66Bdj*{:i/Q=HKpEAhf5pbR9m%LxB)cnUUj2R)X%4");
        const salt: Buffer = Buffer.from("HKA_eVh-%w+x5VVZf,Qdrq9[EYVd:AQKbj$cb@9bh2dL6@@@eRBQQi(/)H[Yr&]c!i5Crv}[[UNZxZfKy+w_x;}FYbg9RtEdd4DF5kt.Tk3[3[_wGz*;jMRK9L;P$;9gaFb,Cj?AtQ+tcWW!zk:-=]CFL!!abbvxTE7y.]K}zC_$xkJqfr8pMry%4h/t?#4.DD6G}3g2x2P]g/Mf9=.]k??L#@YFbzgU3HZdaLkT/Q;YC;y%V+=c%y/;wu{6qnT");

        // Generate key
        pbkdf2(secretKey, salt, 278789, 32, "sha256", (err, derivedKey) => {
            if (err) { res.status(500).json({ message: err }); }
            try {
                // Encrypt body
                const cipher = createCipheriv("aes-256-ecb", derivedKey, Buffer.alloc(0));
                let cipherText = cipher.update(JSON.stringify(req.body.plaintext), "utf-8", "hex");
                cipherText += cipher.final("hex");

                // Decrypt cipherText
                const decipher = createDecipheriv("aes-256-ecb", derivedKey, Buffer.alloc(0));

                let decipheredText = decipher.update(cipherText, "hex", "utf-8");
                decipheredText += decipher.final("utf8");

                res.status(200).json({
                    message: decipheredText
                });
            } catch (err) {
                res.status(500).json({ message: err });
            }

        });
    }

    public plain (req: Request, res: Response) {
        res.status(200).json({
            message: "Berhasil"
        });
    }
}