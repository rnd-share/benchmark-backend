import express, { Application } from "express";
import bodyParser from "body-parser";
import "dotenv/config";
import { BenchmarkController } from "./benchmarkController";

class App {
    public app: Application;
    public port: number;
    public benchmarkController: BenchmarkController;
    
    constructor() {
        this.app = express();
        this.port = 80;
        this.benchmarkController = new BenchmarkController();

        this.app.use(bodyParser.json());

        this.app.post("/v1/sort", this.benchmarkController.bubbleSort);
        this.app.post("/v1/pidigits", this.benchmarkController.piDigits);
        this.app.post("/v1/encrypt", this.benchmarkController.aes);
        this.app.get("/v1/plain", this.benchmarkController.plain);
    }

    public listen() {
        this.app.listen(this.port, () => {
            console.log(`Express (TS) example app running at port ${this.port}`);
        });
    }
}

const app = new App();
app.listen();