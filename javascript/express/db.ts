import { Pool } from "pg"

export default class DB {
    static pool: Pool;

    constructor() {
        DB.getPool();
    }

    public static getPool(): Pool {
        if (!DB.pool) {
            DB.pool = new Pool({
                host: process.env.DB_HOST,
                port: Number(process.env.DB_PORT),
                database: process.env.DB_DATABASE,
                user: process.env.DB_USER,
                password: process.env.DB_PASSWORD,
            });
        }
        return DB.pool;
    }
}