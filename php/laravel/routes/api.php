<?php

use App\Http\Controllers\BenchmarkController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('plain', [BenchmarkController::class, 'plain']);
Route::post('sort', [BenchmarkController::class, 'bubbleSort']);
Route::post('pidigits', [BenchmarkController::class, 'piDigits']);
Route::post('encrypt', [BenchmarkController::class, 'aes']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
