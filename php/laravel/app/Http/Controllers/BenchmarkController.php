<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BenchmarkController extends Controller
{
    public function plain(Request $request)
    {
        return response()->noContent(200);
    }

    public function bubbleSort(Request $request)
    {
        $arr = $request->arr;
        $n = count($arr);

        for ($i = 0; $i < $n - 1; $i++) {
            for ($j = 0; $j < $n - $i - 1; $j++) {
                if ($arr[$j] > $arr[$j + 1]) {
                    list($arr[$j], $arr[$j + 1]) = array($arr[$j + 1], $arr[$j]);
                }
            }
        }
        return response()->json([
            "arr" => $arr
        ], 200);
    }

    public function piDigits(Request $request)
    {
        $numberOfIterations = $request->number;

        $sum = 0;
        $flip = -1;

        for ($i = 1; $i < $numberOfIterations; $i++) {
            $flip *= -1;
            $sum += $flip / (2 * $i - 1);
        }

        return response()->json([
            "res" => $sum * 4
        ], 200);
    }

    public function aes(Request $request)
    {

        $text = $request->plaintext;

        $secretKey = "M:BDru+g(5Y.Mm]bzv9w]R@Sm[&U/3g9u6b;xwW7dqin3C[K,m28mrhJ]=n/CH!pmSLKmL/_B{g&[tDUSq*{c3!3T@#d.QjNnaw4&m_9J.C=+68,kZbKjVNFGp4-2dBVDN)eJpN48;?yuQxv/RE:%a,Tk=.#BbHt7@2..6Z(?MRV-nXVP6(=ARdx;dLiV+TtDZPy]8(7fR@tD4brJR7&Hg66Bdj*{:i/Q=HKpEAhf5pbR9m%LxB)cnUUj2R)X%4";
        $salt = "HKA_eVh-%w+x5VVZf,Qdrq9[EYVd:AQKbj\$cb@9bh2dL6@@@eRBQQi(/)H[Yr&]c!i5Crv}[[UNZxZfKy+w_x;}FYbg9RtEdd4DF5kt.Tk3[3[_wGz*;jMRK9L;P$;9gaFb,Cj?AtQ+tcWW!zk:-=]CFL!!abbvxTE7y.]K}zC_\$xkJqfr8pMry%4h/t?#4.DD6G}3g2x2P]g/Mf9=.]k??L#@YFbzgU3HZdaLkT/Q;YC;y%V+=c%y/;wu{6qnT";

        $iv = openssl_random_pseudo_bytes(16);
        $derivedKey = hash_pbkdf2("sha256", $secretKey, $salt, 278789, 32);

        $cipherText = openssl_encrypt($text, "AES-256-CBC", $derivedKey, OPENSSL_RAW_DATA, $iv);
        $hash = hash_hmac("sha256", $cipherText . $iv, $derivedKey, true);

        $ivHashCiphertext = $iv . $hash . $cipherText;

        $iv = substr($ivHashCiphertext, 0, 16);
        $hash = substr($ivHashCiphertext, 16, 32);
        $cipherText = substr($ivHashCiphertext, 48);

        return response()->json([
            "message" => openssl_decrypt($cipherText, "AES-256-CBC", $derivedKey, OPENSSL_RAW_DATA, $iv)
        ], 200);
    }
}
