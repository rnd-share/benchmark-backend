<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Bubble extends ResourceController
{
    public function bubble()
    {
        $arr = request()->getVar('arr');
        $n = count($arr);


        for ($i = 0; $i < $n-1; $i++) {
            for ($j = 0; $j < $n-$i-1; $j++) {
                if ($arr[$j] > $arr[$j+1]) {
                    list($arr[$j], $arr[$j+1]) = array($arr[$j+1], $arr[$j]);
                }
            }
        }
        return response()
        ->setStatusCode(200)
        ->setJSON([
            "arr" => $arr
        ]);
    }
};
