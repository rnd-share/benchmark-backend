<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Plain extends ResourceController
{
    public function plain()
    {
        return http_response_code(200);
    }
}
