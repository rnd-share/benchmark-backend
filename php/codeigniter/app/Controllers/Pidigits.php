<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;

class Pidigits extends ResourceController
{
    public function Pidigits()
    {
        $numberOfIterations = request()->getVar('number');
        $sum = 0;
        $flip = -1;

        for($i = 1; $i< $numberOfIterations; $i++) {
            $flip *= -1;
            $sum += $flip / (2*$i-1);
        }

        return response()
        ->setStatusCode(200)
        ->setJSON([
            "res" => $sum * 4
        ]);
    }
}
