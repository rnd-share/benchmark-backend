package server

import (
	"benchmark/echo/controller"

	"github.com/labstack/echo/v4"
)

func Init() {
	e := echo.New()
	e.POST("/v1/encrypt", controller.EncryptHandler)
	e.POST("/v1/pidigits", controller.CalculatePiDigits)
	e.POST("/v1/sort", controller.DoBubbleSort)
	e.GET("/v1/plain", controller.ReturnPlain)
	e.Logger.Fatal(e.Start(":80"))
}
