package controller

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type PiDigitsDto struct {
	Number int `json:"number"`
}

func CalculatePiDigits(c echo.Context) error {
	var req PiDigitsDto

	if err := c.Bind(&req); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	sum := 0.0
	flip := -1.0

	for i := 1; i <= req.Number; i++ {
		flip *= -1.0
		sum += flip / float64(2*i-1)
	}

	return c.String(http.StatusOK, strconv.FormatFloat(sum*4.0, 'f', 6, 64))
}
