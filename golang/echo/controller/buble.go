package controller

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type BubbleSortDto struct {
	Arr []int `json:"arr"`
}

func DoBubbleSort(c echo.Context) error {
	var req BubbleSortDto

	if err := c.Bind(&req); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	arr := req.Arr

	n := len(arr)

	for i := 0; i < n-1; i++ {
		for j := 0; j < n-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}

	return c.String(http.StatusOK, "Berhasil")
}
