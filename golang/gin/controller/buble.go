package controller

import (
	"github.com/gin-gonic/gin"

	"net/http"
)

type BubbleSortDto struct {
	Arr []int `json:"arr"`
}

func DoBubbleSort(c *gin.Context) {
	var req BubbleSortDto

	if err := c.ShouldBindJSON(&req); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}

	arr := req.Arr

	n := len(arr)

	for i := 0; i < n-1; i++ {
		for j := 0; j < n-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}

	c.AbortWithStatusJSON(http.StatusOK, arr)
}
