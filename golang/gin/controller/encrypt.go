package controller

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/pbkdf2"
)

type EncryptRequest struct {
	PlainText string `json:"plaintext"`
}

func encryptAES256(key []byte, plaintext string) (string, error) {
	// Create a new AES cipher block using the provided key.
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// Generate a random Initialization Vector (IV).
	iv := make([]byte, aes.BlockSize)
	if _, err := rand.Read(iv); err != nil {
		return "", err
	}

	// Convert the plaintext to a byte slice.
	plaintextBytes := []byte(plaintext)

	// Pad the plaintext to a multiple of the block size.
	paddedPlaintext := pkcs7Pad(plaintextBytes, aes.BlockSize)

	// Create a new cipher block chaining (CBC) mode encrypter.
	mode := cipher.NewCBCEncrypter(block, iv)

	// Encrypt the padded plaintext.
	ciphertext := make([]byte, len(paddedPlaintext))
	mode.CryptBlocks(ciphertext, paddedPlaintext)

	// Combine the ciphertext and IV into a single byte slice.
	ciphertextWithIV := append(iv, ciphertext...)

	// Encode the ciphertext and IV as a base64 string.
	return base64.StdEncoding.EncodeToString(ciphertextWithIV), nil
}

func decryptAES256(key []byte, ciphertext string) (string, error) {
	// Decode the ciphertext and IV from the base64 string.
	ciphertextWithIV, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}

	// Extract the IV from the ciphertext and separate the ciphertext itself.
	iv := ciphertextWithIV[:aes.BlockSize]
	ciphertextBytes := ciphertextWithIV[aes.BlockSize:]

	// Create a new AES cipher block using the provided key.
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// Create a new cipher block chaining (CBC) mode decrypter.
	mode := cipher.NewCBCDecrypter(block, iv)

	// Decrypt the ciphertext.
	plaintext := make([]byte, len(ciphertextBytes))
	mode.CryptBlocks(plaintext, ciphertextBytes)

	// Remove any padding from the plaintext.
	unpaddedPlaintext := pkcs7Unpad(plaintext)

	return string(unpaddedPlaintext), nil
}

// Pad the given byte slice to a multiple of the specified block size using PKCS7 padding.
func pkcs7Pad(data []byte, blockSize int) []byte {
	padding := blockSize - len(data)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padtext...)
}

// Unpad the given byte slice by removing PKCS7 padding.
func pkcs7Unpad(data []byte) []byte {
	padding := int(data[len(data)-1])
	return data[:len(data)-padding]
}

// Gin handler function that encrypts the request body using AES-256 with a hardcoded key.
func EncryptHandler(c *gin.Context) {
	secretKey := []byte("M:BDru+g(5Y.Mm]bzv9w]R@Sm[&U/3g9u6b;xwW7dqin3C[K,m28mrhJ]=n/CH!pmSLKmL/_B{g&[tDUSq*{c3!3T@#d.QjNnaw4&m_9J.C=+68,kZbKjVNFGp4-2dBVDN)eJpN48;?yuQxv/RE:%a,Tk=.#BbHt7@2..6Z(?MRV-nXVP6(=ARdx;dLiV+TtDZPy]8(7fR@tD4brJR7&Hg66Bdj*{:i/Q=HKpEAhf5pbR9m%LxB)cnUUj2R)X%4")
	salt := []byte("HKA_eVh-%w+x5VVZf,Qdrq9[EYVd:AQKbj$cb@9bh2dL6@@@eRBQQi(/)H[Yr&]c!i5Crv}[[UNZxZfKy+w_x;}FYbg9RtEdd4DF5kt.Tk3[3[_wGz*;jMRK9L;P$;9gaFb,Cj?AtQ+tcWW!zk:-=]CFL!!abbvxTE7y.]K}zC_$xkJqfr8pMry%4h/t?#4.DD6G}3g2x2P]g/Mf9=.]k??L#@YFbzgU3HZdaLkT/Q;YC;y%V+=c%y/;wu{6qnT")

	key := pbkdf2.Key(secretKey, salt, 278789, 32, sha256.New)

	// Read the request body
	var req EncryptRequest

	if err := c.ShouldBindJSON(&req); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}

	// Encrypt the request body using AES-256.
	ciphertext, err := encryptAES256(key, req.PlainText)

	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	plaintext, err := decryptAES256(key, ciphertext)
	if err != nil {
		c.AbortWithError(http.StatusBadRequest, errors.New("INVALID CIPHERTEXT"))
		return
	}

	if plaintext == string(req.PlainText) {
		c.AbortWithStatusJSON(http.StatusOK, gin.H{"message": ciphertext})
	} else {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Encryption-Decryption process failed"})
	}

}
