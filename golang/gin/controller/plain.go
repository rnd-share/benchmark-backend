package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ReturnPlain(c *gin.Context) {
	c.AbortWithStatusJSON(http.StatusOK, "success")
}
