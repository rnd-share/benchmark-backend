package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type PiDigitsDto struct {
	Number int `json:"number"`
}

func CalculatePiDigits(c *gin.Context) {
	var req PiDigitsDto

	if err := c.ShouldBindJSON(&req); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
	}

	sum := 0.0
	flip := -1.0

	for i := 1; i <= req.Number; i++ {
		flip *= -1.0
		sum += flip / float64(2*i-1)
	}

	c.AbortWithStatusJSON(http.StatusOK, sum*4.0)
}
