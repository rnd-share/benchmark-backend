package server

import (
	"benchmark/gin/controller"

	"github.com/gin-gonic/gin"
)

func Init() {
	r := gin.Default()
	r.POST("/v1/encrypt", controller.EncryptHandler)
	r.POST("/v1/pidigits", controller.CalculatePiDigits)
	r.POST("/v1/sort", controller.DoBubbleSort)
	r.GET("/v1/plain", controller.ReturnPlain)

	// Start the HTTP server.
	if err := r.Run(":80"); err != nil {
		panic(err)
	}
}
