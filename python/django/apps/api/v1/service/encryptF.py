import os
from Crypto.Cipher import AES
from .. import constants
import hashlib
from Crypto.Util.Padding import pad, unpad


def generate_key(password, salt, iterations):
    key = password + salt
    key = key.encode('utf-8')
    for i in range(iterations):
        key = hashlib.sha256(key).digest()  

    return key

key = generate_key(constants.SECRET_KEY, constants.SALT, constants.ITERATION)


def encrypt(message):
    cipher = AES.new(key, AES.MODE_ECB)

    ciphertext = cipher.encrypt(pad(message.encode('utf-8') , constants.KEY_LENGTH))

    ciphertext_with_salt = constants.SALT.encode('utf-8') + ciphertext
    return ciphertext_with_salt

def decrypt(ciphertext):
    salt = ciphertext[0:255]

    ciphertext_sans_salt = ciphertext[255:]

    cipher = AES.new(key, AES.MODE_ECB)

    padded_plaintext = cipher.decrypt(ciphertext_sans_salt)

    return unpad(padded_plaintext, constants.KEY_LENGTH)

