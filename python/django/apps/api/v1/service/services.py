import os
from .. import constants
import numpy as np

def do_bubble_sort(arr):
    n = len(arr)
    for i in range(n - 1):
        for j in range(n - i - 1):
            if arr[j] > arr[j + 1]:
                temp = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
    return arr

def calculate_pi_digits(n):
    terms = np.arange(0,n)
    series = np.sum((-1)**terms/(2*terms+1))
    return 4*series
