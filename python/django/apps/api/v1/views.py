from rest_framework.response import Response
from rest_framework.decorators import api_view
from .service import services
from .service import encryptF

import json

@api_view(['POST'])
def bublesort(request):
    json_data = json.loads(str(request.body, encoding='utf-8'))
    result = services.do_bubble_sort(json_data["arr"])
    scode = 200    
    return Response(result,scode)

@api_view(['POST'])
def pidigits(request):
    json_data = json.loads(str(request.body, encoding='utf-8'))
    result = services.calculate_pi_digits(json_data["number"])
    scode = 200    
    return Response(result,scode)


@api_view(['GET'])
def plain(request):
    scode = 200    
    return Response("Berhasil",scode)


@api_view(['POST'])
def encrypt(request):
    json_data = json.loads(str(request.body, encoding='utf-8'))
    encryptData = encryptF.encrypt(json_data["plaintext"])
    decryptData = encryptF.decrypt(encryptData)
    res = "success"
    scode = 200
    if decryptData != json_data["plaintext"].encode('utf-8') :
        res = "failed"
        scode = 400
    
    return Response(res,scode)
