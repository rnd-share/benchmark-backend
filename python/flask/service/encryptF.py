import os
from Crypto.Cipher import AES
import hashlib
from Crypto.Util.Padding import pad, unpad

SECRET_KEY = "M:BDru+g(5Y.Mm]bzv9w]R@Sm[&U/3g9u6b;xwW7dqin3C[K,m28mrhJ]=n/CH!pmSLKmL/_B{g&[tDUSq*{c3!3T@#d.QjNnaw4&m_9J.C=+68,kZbKjVNFGp4-2dBVDN)eJpN48;?yuQxv/RE:%a,Tk=.#BbHt7@2..6Z(?MRV-nXVP6(=ARdx;dLiV+TtDZPy]8(7fR@tD4brJR7&Hg66Bdj*{:i/Q=HKpEAhf5pbR9m%LxB)cnUUj2R)X%4";
SALT = "HKA_eVh-%w+x5VVZf,Qdrq9[EYVd:AQKbj$cb@9bh2dL6@@@eRBQQi(/)H[Yr&]c!i5Crv}[[UNZxZfKy+w_x;}FYbg9RtEdd4DF5kt.Tk3[3[_wGz*;jMRK9L;P$;9gaFb,Cj?AtQ+tcWW!zk:-=]CFL!!abbvxTE7y.]K}zC_$xkJqfr8pMry%4h/t?#4.DD6G}3g2x2P]g/Mf9=.]k??L#@YFbzgU3HZdaLkT/Q;YC;y%V+=c%y/;wu{6qnT";
ITERATION = 278789
KEY_LENGTH = 256

def generate_key(password, salt, iterations):
    key = password + salt
    key = key.encode('utf-8')
    for i in range(iterations):
        key = hashlib.sha256(key).digest()  

    return key

key = generate_key(SECRET_KEY, SALT, ITERATION)


def encrypt(message):
    cipher = AES.new(key, AES.MODE_ECB)

    ciphertext = cipher.encrypt(pad(message.encode('utf-8') , KEY_LENGTH))

    ciphertext_with_salt = SALT.encode('utf-8') + ciphertext
    return ciphertext_with_salt

def decrypt(ciphertext):
    salt = ciphertext[0:255]

    ciphertext_sans_salt = ciphertext[255:]

    cipher = AES.new(key, AES.MODE_ECB)

    padded_plaintext = cipher.decrypt(ciphertext_sans_salt)

    return unpad(padded_plaintext, KEY_LENGTH)

