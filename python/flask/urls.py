from django.urls import path
from . import views

urlpatterns = [
    path('sort', views.bublesort),

    path('pidigits', views.pidigits),

    path('plain', views.plain),

    path('encrypt', views.encrypt),


]