from flask import Flask,jsonify,Response,request
from service import services
from service import encryptF

import json

app = Flask(__name__)
url = "/v1/"

@app.route(url+'sort',methods=['POST'])
def bublesort():
    json_data = request.get_json()
    data = json_data.get('arr')
    print(type(data))
    print(len(data))
    result = services.do_bubble_sort(data,len(data))
    scode = 200    
    return Response(result,scode)

@app.route(url+'pidigits',methods=['POST'])
def pidigits():
    json_data = request.get_json()
    scode = 200
    pidigits = services.calculate_pi_digits(json_data.values["number"])
    return Response(pidigits,status=scode)

@app.route(url+'plain',methods=['GET'])
def plain():
    scode = 200    
    return Response("Berhasil",scode)


@app.route(url+'encrypt',methods=['POST'])
def encrypt():
    json_data = request.get_json()
    encryptData = encryptF.encrypt(json_data["plaintext"])
    decryptData = encryptF.decrypt(encryptData)
    res = "success"
    scode = 200
    if decryptData != json_data["plaintext"].encode('utf-8') :
        res = "failed"
        scode = 400
    
    return Response(decryptData,scode)

if __name__ == '__main__':
	app.run(host='0.0.0.0',port=80,debug=True, use_reloader=False)

